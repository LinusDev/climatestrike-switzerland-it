# Notes on the new main site for climatestrike switzerland

## Technical
### Wordpress Setup
- Wordpress Multisite for different regional groups
    - Use subdomains for the different sites (i.e. zh.climatestrike.ch or zuerich.climatestrike.ch)
- Multilingual support with **WPML** / Polylang
    - Urls use directories for languages if the site is multilingual and not the default language is selected (i.e. climatestrike.ch/fr/events or bern.climatestrike.ch/it/info)
    - Slugs are in english (only if the site is multilingual)

### Conventions
## Accessibility
- Always add alternative text and description to pictures


### Tasks
- Copy content to new site (Rebuild with divi)
- Get WPML
- Translate content
- Find tagline
- Add font awesome icons

## Who?
- Linus